''' Helper functions...'''
from datetime import datetime, timedelta
import json
import logging
import os

DEBUG = logging.DEBUG
INFO = logging.INFO

CACHING_FILE = 'data/bitbucket_dump.json'
LOG_FILE = 'logs/bitbucket.log' 


def rm_log():
    ''' Function to delete Log File. '''
    if os.path.isfile(LOG_FILE):
        os.remove(LOG_FILE)

def get_log(name, log_lvl=INFO, file_log=True, file_log_lvl=DEBUG):
    ''' Method to get a logger. '''
    log = logging.getLogger(name)
    log.setLevel(DEBUG)
    form = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    sth = logging.StreamHandler()
    sth.setLevel(log_lvl)
    sth.setFormatter(form)
    log.addHandler(sth)
    if file_log:
        fth = logging.FileHandler(LOG_FILE)
        fth.setLevel(file_log_lvl)
        fth.setFormatter(form)
        log.addHandler(fth)
    return log


def is_expired(date):
    ''' Check if data is expired. '''
    past = datetime.now() - timedelta(minutes=60)
    if past > date:
        return True
    return False
    

def write_json(data_dict):
    ''' Write Json to File. '''
    with open(CACHING_FILE, 'w', encoding='utf-8') as f:
        json.dump(data_dict, f, ensure_ascii=False, indent=4)


def read_json():
    ''' Read Json from File. '''
    with open(CACHING_FILE, 'r') as f:
        data_dict = json.loads(f.read())
    return data_dict
