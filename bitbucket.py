# Module to configure Bitbucket

from pprint import pprint


from requests import Session
from requests.auth import HTTPBasicAuth
import os


from helper import get_log, write_json, read_json, CACHING_FILE
from test import GET_INTERNAL_URL, URL, PAYLOAD_DEVELOP, PAYLOAD_MASTER


BASE_URL = 'https://api.bitbucket.org/'
GET_WORKSPACES = '2.0/workspaces'


class BitbucketConn:
    ''' Class to define conn to Bitbucket Cloud. '''

    def __init__(self, username, password):
        ''' __ini__ Method. '''
        self.conn = self.get_conn(username, password)
        self.log = get_log(__name__)
        self.req_nr = 0
        self.workspaces = []
        self.test_repo = None
        self.test_data = []

    def __call__(self):
        ''' __call__ Magic Method... '''
        self.log.info('Bitbucket has been called...')
        if not os.path.isfile(CACHING_FILE): 
            self.log.info('Getting Workspaces from Bitbucket Cloud...')
            self.get_base_data()
        else:
            self.log.info('Getting Workspaces from Caching File...')
            self.workspaces = read_json()
        write_json(self.workspaces) 
        self.print_data()
        self.test_internal_url()

    def get_conn(self, username, password):
        ''' Open a Session. '''
        session = Session()
        session.auth = HTTPBasicAuth(username, password)  
        return session

    def get_base_data(self):
        ''' getting basic data  '''
        self.get_workspaces()
        self._get_owners()
        self._get_projects()

    def print_data(self):
        ''' Print Data...'''
        proj_nr = sum([len(workspace['projects']) for workspace in self.workspaces])
        
        repo_nr = 0
        for workspace in self.workspaces:
            for project in workspace['projects']:
                repo_nr += len(project['repos']) 

        self.log.info('###===--->>> Total nr of Workspaces: %s', len(self.workspaces))
        self.log.info('###===--->>> Total nr of Projects: %s', proj_nr)
        self.log.info('###===--->>> Total nr of Repositories: %s', repo_nr)
        self.log.info('###===--->>> Total nr of Requests: %s', self.req_nr)

    def get_data(self, url):
        ''' Get Data... '''
        data = self.conn.get(url)
        self.req_nr += 1
        return data.json() 

    def put_data(self, url, payload):
        ''' Put Data... '''
        response = self.conn.put(url, data=payload)
        self.req_nr += 1
        return response

    def _get_data_page(self, url):
        ''' Get method to deal with pagination... '''
        data = self.get_data(url)
        yield data['values'] 
        if not 'next' in data.keys():
            return
        page_len = int(data['size'] / data['pagelen']) + 1
        for page in range(2, page_len + 1):
            yield self.get_data(data['next'])['values'] 

    def get_workspaces(self):
        ''' Get workspaces Data... '''  
        workspaces = self.get_data(BASE_URL + GET_WORKSPACES)
        for workspace in workspaces['values']:
            self.workspaces.append({
                "name": workspace['name'],
                "slug": workspace['slug'],
                "owners": [],
                "owners_link": workspace['links']['owners']['href'],
                "projects_link": workspace['links']['projects']['href'],
                "projects": []                   
            })

    def _get_owners(self):
        ''' Getting owners of workspaces... '''
        for workspace in self.workspaces:
            self.log.info('Getting Owners for Workspace: %s...', workspace['name'])
            owners = self.get_data(workspace['owners_link'])
            for item in owners['values']:
                workspace['owners'].append({
                    "type": item['type'],
                    "name": item['user']['display_name'],
                    "account_id": item['user']['account_id']
                })

    def _get_projects(self):
        ''' Getting Projects from Workspaces... '''
        for workspace in self.workspaces:
            self.log.info('Getting Projects for Workspace: %s...', workspace['name'])
            projects = self.get_data(workspace['projects_link'])
            for item in projects['values']:
                repos_url = item['links']['repositories']['href']
                workspace['projects'].append({
                    "key": item['key'],
                    "name": item['name'],
                    "repo_link": item['links']['repositories']['href'],
                    "repos": self._get_repositories(repos_url, item['name'])# if repos_url else None
                })

    def _get_repositories(self, link, proj_name):
        ''' Getting Repositories for Projects... '''
        data = []
        self.log.info('Getting Repos for Project: %s...', proj_name)
        for data_set in self._get_data_page(link):
            for repo in data_set:
                data.append({
                    "name": repo['name'],
                    "full_name": repo['full_name'],
                    "main_branch": repo['mainbranch']['name'],
                    "slug": repo['slug'],
                    "branches": [],
                    "branches_links": repo['links']['branches']['href']
                })
        return data

    def test_internal_url(self):
        ''' testing... '''
        response = self.get_data(GET_INTERNAL_URL)
        print(f'#### {response.content}')









