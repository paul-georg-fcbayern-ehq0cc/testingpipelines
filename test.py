# Module for testing...

URL = 'https://bitbucket.org/!api/internal/repositories/paul-georg-fcbayern-ehq0cc/testingpipelines/branch-restrictions/by-branch-type/production'
GET_INTERNAL_URL = 'https://bitbucket.org/!api/2.0/repositories/paul-georg-fcbayern-ehq0cc/testingpipelines/refs/branches/main'



PAYLOAD_DEVELOP = {
    "values":[{
        "kind":"push",
        "branch_match_kind":"branching_model",
        "branch_type":"development",
        "users":[],
        "groups":[{
            "slug":"administrators"
        }]
        },{
            "kind":"delete",
            "branch_match_kind":"branching_model",
            "branch_type":"development"
        },{
            "kind":"require_tasks_to_be_completed",
            "branch_match_kind":"branching_model",
            "branch_type":"development"
        },{
            "kind":"require_no_changes_requested",
            "branch_match_kind":"branching_model",
            "branch_type":"development"
        },{
            "kind":"reset_pullrequest_changes_requested_on_change",
            "branch_match_kind":"branching_model",
            "branch_type":"development"
        },{
            "kind":"require_approvals_to_merge",
            "value":1,
            "branch_match_kind":"branching_model",
            "branch_type":"development"
        },{
            "kind":"require_passing_builds_to_merge",
            "value":1,
            "branch_match_kind":"branching_model",
            "branch_type":"development"
        }]
}


PAYLOAD_MASTER = {
    "values":[{
        "kind":"push",
        "branch_match_kind":"branching_model",
        "branch_type":"production",
        "users":[],
        "groups":[]
        },{
            "kind":"delete",
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        },{
            "kind":"force",
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        },{
            "kind":"require_tasks_to_be_completed",
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        },{
            "kind":"require_no_changes_requested",
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        },{ 
            "kind":"reset_pullrequest_changes_requested_on_change",
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        },{
            "kind":"require_approvals_to_merge",
            "value":1,
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        },{
            "kind":"require_passing_builds_to_merge",
            "value":1,
            "branch_match_kind":"branching_model",
            "branch_type":"production"
        }]
}
