''' This is the main Entrypoint... '''

from bitbucket import BitbucketConn
from helper import get_log, write_json, read_json, rm_log


PASS = 'xAMkdWUphKjdc6Yvj9aa'
URL = 'https://bitbucket.org/'
USER = 'paul-georg-fcbayern-eHQ0cC'


def main():
    ''' Main Method to be called at start. '''
    rm_log()
    log = get_log(__name__)
    log.info('Implementing this...')
    #import pdb; pdb.set_trace()
    x = BitbucketConn(USER, PASS)
    x()


if __name__ == '__main__':
    main()
